import os
from flask import Flask, jsonify, request, flash, redirect, url_for, session, send_from_directory, send_file
from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin
import sys
sys.path.insert(0, '../Scripts/')
from  Hazards_Batch import Hzd_Batch
from  Themes_Batch import Thm_Batch
from  Hazards_One import Hzd_One
from  Themes_One import Thm_One


import pandas as pd

UPLOAD_FOLDER = "../Uploads"
RESULT_FOLDER = '../Results'

ALLOWED_EXTENSIONS = set(['csv'])

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['RESULT_FOLDER'] = RESULT_FOLDER
cors = CORS(app, resources={r"/*": {"origins": "*"}})


response = []


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

@app.route('/download/file', methods=['GET'])
def download_file():
    path = "../Results/results.csv"
    return send_file(path, as_attachment=True)

@app.route('/delete/file', methods=['POST', 'GET'])
@cross_origin()
def delete_file():
    Upload_filename = secure_filename('upload.csv')
    Download_filename = secure_filename('results.csv')
    Download_jsonfile = secure_filename('results.json')

    uploaded_csv = os.path.exists("../Uploads/upload.csv")
    downloaded_csv = os.path.exists("../Results/results.csv")
    downloaded_json = os.path.exists("../Results/results.json")
    # os.remove(os.path.join(app.config['UPLOAD_FOLDER'], Upload_filename))
    if uploaded_csv:
        if downloaded_csv and downloaded_json:
            os.remove(os.path.join(app.config['UPLOAD_FOLDER'], Upload_filename))
            os.remove(os.path.join(app.config['RESULT_FOLDER'], Download_filename))
            os.remove(os.path.join(app.config['RESULT_FOLDER'], Download_jsonfile))
            return 'I am all cleared'
        
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], Upload_filename))
        return 'I am in upload'
    if downloaded_csv and downloaded_json:
        os.remove(os.path.join(app.config['RESULT_FOLDER'], Download_filename))
        os.remove(os.path.join(app.config['RESULT_FOLDER'], Download_jsonfile))
        return 'I am in results'
        
    return 'Nothing'

@app.route('/upload/file', methods=['POST', 'GET'])
@cross_origin()
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename('upload.csv')
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # return redirect(url_for('uploaded_file',
            #                         filename=filename))
            return 'successful'
        
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''


@app.route('/getResults/hazard/batch', methods=['GET'])
def hazardBatchResults():
    uploaded_csv = os.path.exists("../Uploads/upload.csv")
    if uploaded_csv:
        df1 = pd.read_csv('../uploads/upload.csv',encoding = 'Latin-1')
        Results = Hzd_Batch(df1)
        return Results
    else:
        return "File doesn't exit"
    

@app.route('/getResults/theme/batch', methods=['GET'])
def themeBatchResults():
    df1 = pd.read_csv('../uploads/upload.csv',encoding = 'Latin-1')
    Results = Thm_Batch(df1)
    return Results


@app.route('/getResults/hazard/response', methods=['POST'])
@cross_origin()
def hazardRequest():
    request.get_json(force=True)
    requestResponse = [request.json['title'],request.json['textData']]
    if not requestResponse[1] and not requestResponse[0]:
        return "title and textdata"

    if not requestResponse[1]:
        return "textData"
    if not requestResponse[0]:
        return "title"
    
    Results = Hzd_One(requestResponse[1], requestResponse[0])
    return Results
    
@app.route('/getResults/theme/response', methods=['POST'])
def themeRequest():
    request.get_json(force=True)
    requestResponse = [request.json['title'],request.json['textData']]
    if not requestResponse[1] and not requestResponse[0]:
        return "title and textdata"

    if not requestResponse[1]:
        return "textData"
    if not requestResponse[0]:
        return "title"
    
    Results = Thm_One(requestResponse[1], requestResponse[0])
    return Results

@app.route('/', methods=['GET'])
def test():
    return redirect(url_for(delete_file))



if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.run(debug=True)


# @app.route('/languages', methods=['GET'])
# @cross_origin()
# def returnAll():
#     return jsonify({'languages': languages})




# @app.route('/languages/<string:name>', methods=['PUT'])
# def editOne(name):
#     request.get_json(force=True)
#     langs = [language for language in languages if language['name'] == name]
#     langs[0]['name'] = request.json['name']
#     return jsonify({'language': langs[0]})


# @app.route('/languages/<string:name>', methods=['GET'])
# @cross_origin()
# def returnOne(name):
#     langs = [language for language in languages if language['name'] == name]
#     return jsonify({'language': langs})

# @app.route('/languages', methods=['GET', 'POST'])
# @cross_origin()
# def addOne():
#     request.get_json(force=True)
#     language = {'name': request.json['name'] }
#     languages.append(language)
#     return jsonify({'languages': languages})