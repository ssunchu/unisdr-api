def Hzd_Batch(x):
    import re
    import numpy as np
    import pandas as pd
    from nltk.corpus import stopwords
    from nltk.stem import PorterStemmer
    from textblob import Word
    stop = stopwords.words('english')
    import pickle
    import enchant
    import os
    #import Data
    ##Request-Response
    df1 = x
    df1['text_data'] = df1['title']+df1['textData']
    # Preprocessing data
    def clean_text(text):
        text = text.lower()
        text = re.sub(r"what's", "what is ", text)
        text = re.sub(r"\'s", " ", text)
        text = re.sub(r"\'ve", " have ", text)
        text = re.sub(r"can't", "can not ", text)
        text = re.sub(r"n't", " not ", text)
        text = re.sub(r"i'm", "i am ", text)
        text = re.sub(r"\'re", " are ", text)
        text = re.sub(r"\'d", " would ", text)
        text = re.sub(r"\'ll", " will ", text)
        text = re.sub(r"\'scuse", " excuse ", text)
        text = re.sub('\W', ' ', text)
        text = re.sub('\s+', ' ', text)
        text = text.strip(' ')
        return text
    df1['preprocessed_text'] = df1['text_data'].map(lambda com : clean_text(com))
    #Lower case
    df1['preprocessed_text'] = df1['preprocessed_text'].apply(lambda x: " ".join(x.lower() for x in x.split()))
    cleaned_text = df1['preprocessed_text']
    def preprocessed_data(clean_text):
        #Remove Punctuation
        clean_text = clean_text.str.replace('[^\w\s]','')
        #Remove non English text
        d = enchant.Dict("en_US")
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if d.check(x)))
        #Remove stop words
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if x not in stop))
        #Remove common words
        freq1 = pd.Series(' '.join(clean_text).split()).value_counts()[:10]
        freq1 = list(freq1.index)
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if x not in freq1))
        #Remove rare words
        freq2 = pd.Series(' '.join(clean_text).split()).value_counts()[-10:]
        freq2 = list(freq2.index)
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if x not in freq2))
        #Remove the digits
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if not x.isdigit()))
        #Stemming the words
        from nltk.stem import PorterStemmer
        st = PorterStemmer()
        clean_text = clean_text.apply(lambda x: " ".join([st.stem(word) for word in x.split()]))
        #Lemmatization
        from textblob import Word
        clean_text = clean_text.apply(lambda x: " ".join([Word(word).lemmatize() for word in x.split()]))
        return clean_text

    ready_text = preprocessed_data(cleaned_text)
    categories = ['Avalanche','Cold Wave',
     'Cyclone','Drought','Earthquake','Epidemic & Pandemic',
     'Flood','Heat Wave','Insect Infestation',
     'Land Slide','NBC - Nuclear & Biological & Chemical','Storm Surge',
     'Technical Disaster','Tornado','Tsunami','Volcano','Wild Fire']
    output = []
    def prediction(inputData):
        #loaded all the trained models
        for i in range(0,17):
            loaded_model = pickle.load(open(os.path.expanduser(r'../Models/Hazard/'+categories[i]+'_model.sav'),'rb'))
            test_label = loaded_model.predict(inputData)
            output.append(test_label)
        return output
    def predicted_probability(inputData2):
        all_prob = pd.DataFrame(np.zeros((len(cleaned_text),1)))
        for i in range(0,17):
            loaded_model = pickle.load(open(os.path.expanduser(r'../Models/Hazard/'+categories[i]+'_model.sav'),'rb'))
            clf = loaded_model
            prob = clf.predict_proba(inputData2)
            df_prob = pd.DataFrame(prob)
            all_prob = pd.concat([all_prob,df_prob],axis =1)
        return all_prob

    output_arr = prediction(cleaned_text)
    prediction_output  = pd.DataFrame(output_arr).T
    prediction_proba = predicted_probability(cleaned_text)
    prediction_proba = prediction_proba.iloc[:,1:]
    prediction_proba = prediction_proba.applymap(lambda x : int(x*100))
    prediction_proba.columns = range(0,34)
    prediction_proba = prediction_proba.astype(str)+"%"
    even_col = range(0,34,2)
    prediction_probability = prediction_proba.drop([x for x in even_col],axis = 1)
    df = np.zeros((len(prediction_output),17))
    df_zero = pd.DataFrame(df)
    for col in range(0,17):
        for row in range(0,len(prediction_output)):
            if prediction_output.iloc[row,col] == 1:
                df_zero.iloc[row,col] = prediction_probability.iloc[row,col]

    for i in range(17):
        prediction_output.iloc[:,i] = prediction_output.iloc[:,i].map({1: categories[i],0:str(0)})

    result_label = prediction_output.values.tolist()
    for i in result_label:
        while "0" in i:
            i.remove("0") 
    pred_proba = df_zero.values.tolist()
    for i in pred_proba:
        while 0.0 in i:
            i.remove(0.0)
    # Change output format

    result_arr = np.asarray(result_label)
    result_str = pd.DataFrame(result_arr).astype(str) 
    
    proba_arr = np.asarray(pred_proba)
    proba_str = pd.DataFrame(proba_arr).astype(str)
   
    result_idx = result_str[0].map(lambda x: re.sub('\'','',x)).map(lambda x: re.sub(']','',x)).map(lambda x: re.sub('\[','',x))
    result_idx = result_idx.to_frame('labels')

    proba_idx = proba_str[0].map(lambda x: re.sub('\'','',x)).map(lambda x: re.sub(']','',x)).map(lambda x: re.sub('\[','',x))
    proba_idx = proba_idx.to_frame('probability')
    result_model = pd.concat([df1['title'],df1['textData'],result_idx['labels'],proba_idx['probability']],axis = 1)
    for i in range(0,len(result_model)):
        if result_model.iloc[i,2] == '':
            result_model.iloc[i,2] = 'No Results'
        if result_model.iloc[i,3] == '':
                result_model.iloc[i,3] = 'No Probability'
                
    #Lower case
    df1['preprocessed_title'] = df1['title'].apply(lambda x: " ".join(x.lower() for x in x.split()))
    cleaned_title = df1['preprocessed_title']
    ready_title = preprocessed_data(cleaned_title)
    # Whole data sheet for tagging no label
    result_model = pd.concat([df1['title'],df1['textData'],result_idx['labels'],proba_idx['probability'],ready_text,ready_title],axis = 1)
    ##Adding 1226
    for i in range(0,len(result_model)):
        if result_model.iloc[i,2] == '':
            result_model.iloc[i,2] = 'No Results'
        if result_model.iloc[i,3] == '':
                result_model.iloc[i,3] = 'No Probability'
    no_label_step1 = result_model.loc[result_model['labels'] == "No Results"]

    dic = {'Avalanche':'avalanche', 
    'Cold Wave': 'cold wave',
    'Cyclone':'cyclone',
    'Drought':'drought',
    'Earthquake':'earthquake',
    'Flood':'flood',
    'Heat Wave':'heat wave',
    'Land Slide': 'land slide',
    'Storm Surge':'storm',
    'Technical Disaster':'technical',
    'Tornado':'tornado',
    'Tsunami': 'tsunami',
    'Wild Fire':'fire',
    ## With Elements
    'Epidemic & Pandemic':('epidemic','pandemic'),
    'Insect Infestation':('insect','infestation'),
    'NBC - Nuclear & Biological & Chemical':('nuclear','chemical','biology'),
    'Volcano':('volcanic','volcano')}
    categories = ['Avalanche','Cold Wave','Cyclone','Drought','Earthquake','Flood','Heat Wave',
    'Land Slide','Storm Surge','Technical Disaster','Tornado','Tsunami','Wild Fire',
    ## With Elements
    'Epidemic & Pandemic','Insect Infestation',
    'NBC - Nuclear & Biological & Chemical','Volcano']
###########################################

    print('*****1*******')
    ## One kw label--title
    length1 = len(no_label_step1)
    for i in range(0,length1):
            for e in range(0,13):
                if dic[str(categories[e])] in no_label_step1.iloc[i,5]:
                    no_label_step1.iloc[i,2] = str(categories[e])
                    no_label_step1.iloc[i,3] = '80%'
    ## multi kw label--title
    print('*****2*******')
    for i in range(0,length1):
        for e in range(13,17):
            for element in dic[str(categories[e])]:
                if element in no_label_step1.iloc[i,5]:
                    no_label_step1.iloc[i,2] = str(categories[e])
                    no_label_step1.iloc[i,3] = '80%'
    print('*****3*******')
###################################### 
    '''
    no_label_step2 = no_label_step1.loc[no_label_step1['labels'] == "No Results"]
    kw_df = pd.read_csv('KW_listV2.csv')
    summary = []

    for text in no_label_step2['preprocessed_text']:
        table = []
        for i in range(0,17):
            count = text.count(str(kw_df.iloc[0,i]))+text.count(str(kw_df.iloc[1,i]))+text.count(str(kw_df.iloc[2,i]))
            table.append(count)
        summary.append(table)

###########################################
    summary = pd.DataFrame(summary)
    summary.columns = categories
    summary['Labels'] = summary.idxmax(axis = 1)
    summary['check_zero'] = summary.max(axis=1)
    length2 = len(summary)
    for i in range(0,length2):
        if summary.iloc[i,18] == 0:
            summary.iloc[i,17] = 'No Results'
    no_label_step2 = no_label_step2.reset_index()
    no_label_step2 = pd.concat([no_label_step2,summary],axis = 1)
    no_label_step2['labels'] = no_label_step2['Labels']
    no_label_step2 = pd.concat([no_label_step2['title'],no_label_step2['textData'],no_label_step2['labels'],no_label_step2['probability']],axis =1 )
    for i in range(0,len(no_label_step2)):
        if no_label_step2.iloc[i,2] != 'No Results':
            no_label_step2.iloc[i,3] = '50%'
'''
    result_model = result_model.loc[result_model['labels'] != 'No Results']
    result_model = pd.concat([result_model['title'],result_model['textData'],result_model['labels'],result_model['probability']],axis =1 )
    #result_model.shape
    #no_label_step1 = no_label_step1.loc[no_label_step1['labels'] != 'No Results']
    no_label_step1 = pd.concat([no_label_step1['title'],no_label_step1['textData'],no_label_step1['labels'],no_label_step1['probability']],axis =1 )
    #no_label_step1.shape
    #results = pd.concat([result_model,no_label_step1,no_label_step2])
    results = pd.concat([result_model,no_label_step1])
    result_model = results.reset_index().drop(['index'],axis = 1 )
    
    result_T = result_model.T
    result_file = result_model.to_json("../Results/results.json")
    result_csv = result_model.to_csv("../Results/results.csv")
    results = result_T.to_json(orient = "columns")
    return results