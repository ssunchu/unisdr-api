#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 11:31:47 2018

@author: yihanbao
"""
def Thm_Batch(x):
    import re
    import numpy as np
    import pandas as pd
    from nltk.corpus import stopwords
    from nltk.stem import PorterStemmer
    from textblob import Word
    stop = stopwords.words('english')
    import pickle
    import enchant
    import os
    # Import data
    df1 = x
    df1['text_data'] = df1['title']+df1['textData']
    # Preprocess data
    def clean_text(text):
        text = text.lower()
        text = re.sub(r"what's", "what is ", text)
        text = re.sub(r"\'s", " ", text)
        text = re.sub(r"\'ve", " have ", text)
        text = re.sub(r"can't", "can not ", text)
        text = re.sub(r"n't", " not ", text)
        text = re.sub(r"i'm", "i am ", text)
        text = re.sub(r"\'re", " are ", text)
        text = re.sub(r"\'d", " would ", text)
        text = re.sub(r"\'ll", " will ", text)
        text = re.sub(r"\'scuse", " excuse ", text)
        text = re.sub('\W', ' ', text)
        text = re.sub('\s+', ' ', text)
        text = text.strip(' ')
        return text
    df1['preprocessed_text'] = df1['text_data'].map(lambda com : clean_text(com))
    #Lower case
    df1['preprocessed_text'] = df1['text_data'].apply(lambda x: " ".join(x.lower() for x in x.split()))
    cleaned_text = df1['preprocessed_text']
    def preprocessed_data(clean_text):
        #Remove Punctuation
        clean_text = clean_text.str.replace('[^\w\s]','')
        #Remove stop words
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if x not in stop))
        #Remove common words
        freq1 = pd.Series(' '.join(clean_text).split()).value_counts()[:10]
        freq1 = list(freq1.index)
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if x not in freq1))
        #Remove rare words
        freq2 = pd.Series(' '.join(clean_text).split()).value_counts()[-10:]
        freq2 = list(freq2.index)
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if x not in freq2))
        #Remove the digits
        clean_text = clean_text.apply(lambda x: " ".join(x for x in x.split() if not x.isdigit()))
        #Stemming the words
        from nltk.stem import PorterStemmer
        st = PorterStemmer()
        clean_text[:5].apply(lambda x: " ".join([st.stem(word) for word in x.split()]))
        #Lemmatization
        from textblob import Word
        clean_text = clean_text.apply(lambda x: " ".join([Word(word).lemmatize() for word in x.split()]))
        return clean_text
    cleaned_text = preprocessed_data(cleaned_text)
    categories = ['Advocacy & Media','Capacity Development','Children and Youth','Civil Society & NGOs','Climate Change',
     'Community-based DRR','Complex Emergency','Critical Infrastructure','Cultural Heritage','Disaster Risk Management','Early Warning',
     'Economics of DRR','Education & School Safety','Environment & Ecosystems',
     'Food Security & Agriculture','GIS & Mapping','Gender','Governance','Health & Health Facilities',
     'Human Mobility','Indigenous Knowledge','Information Management','Insurance & Risk Transfer','Private Sector','Recovery',
     'Risk Identification & Assessment','Science and Technology','Small Islands Developing States (SIDS)',
     'Social Impacts & Social Resilience','Space & Aerial Technology','Structural Safety','Urban Risk & Planning','Vulnerable Populations','Water']
    # Load trained model get the predicted label and the probabilities
    output = []
    def prediction(inputData):
        #loaded all the trained models
        for i in range(0,34):
            loaded_model = pickle.load(open(os.path.expanduser(r'../Models/Theme/'+categories[i]+'_model.sav'),'rb'))   
            test_label = loaded_model.predict(inputData)
            output.append(test_label)
        return output
    def predicted_probability(inputData2):
        all_prob = pd.DataFrame()
        for i in range(0,34):
            loaded_model = pickle.load(open(os.path.expanduser(r'../Models/Theme/'+categories[i]+'_model.sav'),'rb'))   
            clf = loaded_model
            prob = clf.predict_proba(inputData2)
            df_prob = pd.DataFrame(prob)
            all_prob = pd.concat([all_prob,df_prob],axis =1)
        return all_prob
    output_arr = prediction(cleaned_text)
    prediction_output  = pd.DataFrame(output_arr).T
    prediction_proba = predicted_probability(cleaned_text)## Should be dataframe
    prediction_proba.columns = range(0,68)
    prediction_proba = prediction_proba.applymap(lambda x: "{0:.2f}%".format(x*100))
    even_col = range(0,68,2)
    prediction_probability = prediction_proba.drop([x for x in even_col],axis = 1)
    df = np.zeros((len(prediction_output),34))
    df_zero = pd.DataFrame(df)
    for col in range(0,34):
        for row in range(0,len(prediction_output)):
            if prediction_output.iloc[row,col] == 1:
                df_zero.iloc[row,col] = prediction_probability.iloc[row,col]
    for i in range(34):
        prediction_output.iloc[:,i] = prediction_output.iloc[:,i].map({1: categories[i],0:str(0)})
        
    result_label = prediction_output.values.tolist()
    for i in result_label:
        while "0" in i:
            i.remove("0")         
    pred_proba = df_zero.values.tolist()
    for i in pred_proba:
        while 0.0 in i:
            i.remove(0.0) 
            
    result_arr = np.asarray(result_label)
    result_str = pd.DataFrame(result_arr).astype(str)
    proba_arr = np.asarray(pred_proba)
    proba_str = pd.DataFrame(proba_arr).astype(str)
    result_idx = result_str[0].map(lambda x: re.sub('\'','',x)).map(lambda x: re.sub(']','',x)).map(lambda x: re.sub('\[','',x))
    result_idx = result_idx.to_frame('labels')
    proba_idx = proba_str[0].map(lambda x: re.sub('\'','',x)).map(lambda x: re.sub(']','',x)).map(lambda x: re.sub('\[','',x))
    proba_idx = proba_idx.to_frame('probability')
    result_model = pd.concat([df1['title'],df1['textData'],result_idx['labels'],proba_idx['probability']],axis = 1)
    for i in range(0,len(result_model)):
        if result_model.iloc[i,2] == '':
            result_model.iloc[i,2] = 'No Results'
        if result_model.iloc[i,3] == '':
                result_model.iloc[i,3] = 'No Probability'
    result_T = result_model.T
    
    result_file = result_model.to_json("../Results/results.json")
    result_csv = result_model.to_csv("../Results/results.csv")
    results = result_T.to_json(orient = "columns")
    return results

    '''
    output = []
    def prediction(inputData):
        #loaded all the trained models
        for i in range(0,34):
            loaded_model = pickle.load(open(os.path.expanduser(r'../Models/Theme/'+categories[i]+'_model.sav'),'rb'))   
            test_label = loaded_model.predict(inputData)
            output.append(test_label)
        return output
    
    output_arr = prediction(df1['preprocessed_text'])
    
    prediction_output  = pd.DataFrame(output_arr).T
    for i in range(34):
        prediction_output.iloc[:,i] = prediction_output.iloc[:,i].map({1: categories[i],0:str(0)})
        
    result_label = prediction_output.values.tolist()
    for i in result_label:
        while "0" in i:
            i.remove("0")          
    result_arr = np.asarray(result_label)
    result_str = pd.DataFrame(result_arr).astype(str)
    result_idx = result_str[0].map(lambda x: re.sub('\'','',x)).map(lambda x: re.sub(']','',x)).map(lambda x: re.sub('\[','',x))
    result_idx = result_idx.to_frame('Labels')
    result = pd.concat([df1['title'],df1['textData'],result_idx['Labels']],axis = 1)
    length = len(result.iloc[:,2])
    for i in range(0,length):
        if result.iloc[i,2] == "":
            result.iloc[i,2] = "No Results"
    result_T = result.T
    result_file_T = result_T.to_json("../Results/results.json")
    result_csv = result.to_csv("../Results/results.csv")
    results= result_T.to_json(orient = "columns")
    return results
    '''