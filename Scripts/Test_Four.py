#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 13:53:57 2018

@author: yihanbao
"""



from Hazards_Batch import Hzd_Batch
Hzd_Batch('inputData_hzd.csv')


# from Themes_Batch import Thm_Batch
# Thm_Batch('inputData_thm.csv')

# from Hazards_One import Hzd_One
# Hzd_One('<Em> International Workshop organized by the Czech Hydrometeorogical Institute and the Czech National Committee for Disaster Reduction, November 1 to 2, 2020, Prague. </Em>  This document reports on a workshop which brought together experienced people directly involved in facing Flash Floods (FFs) like forecasters from National Meteorological and Hydrological Services (NMHSs), Civil Defense or Fire and Rescue Services, other parts of crisis management systems, local administration and the public, to find possibilities of improvement of efficiency and diminishing an impact of such fast and dangerous events. It provides a comparison of existing systems for early warning and protection against FFs in the European Network of National Platforms (ENNP) countries (France, Germany, Poland and the Czech Republic) and the United States. It is intended to stimulate discussion in order to find significant improvement of early warning, dissemination and preparedness for these extremely quick floods. It advocates for the role of national platforms for disaster risk reduction to be directed towards modern and complex protection against FFs leading to minimization of losses of lives and property in their respective countries.','Early warning for flash floods')

# from Themes_One import Thm_One
# Thm_One('<em>A Practitioner?s perspective on disaster risk management in Latin America and the Caribbean, January 2014:</em>This issue addresses the increased exposure of cities with population sizes of less than one million in the Latin America and Caribbean region to floods and landslides due to rapid urbanization. It presents a regional initiative piloted by the World Bank?s Latin America and Caribbean Disaster Risk and Urban Development team to equip municipal governments with decision-making tools and capacity building that are needed to integrate climate change adaptationinto urban development planning for small and medium-sized cities.',
#              'Insights in DRM, issue 3: climate change adaptation planning in small and medium-sized cities')



